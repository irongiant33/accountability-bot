// .js file includes
const fs = require('fs')
const reportReply = require(process.env.REPLY)
const setupAccountability = require(process.env.ACCOUNTABILITY)
const missingReport = require(process.env.MISSING)

// tests user messages for valid accountability report strings
function report_keyword_test(message)
{
	const message_content = message.content.toLowerCase()	
	const up_check = new RegExp("^(<@!\d+> up)|(up)$")
	const alive_check = new RegExp("^(<@!\d+> alive)|(alive)$")
	const here_check = new RegExp("^(<@!\d+> here)|(here)$")
	
	const up_bool = up_check.test(message_content)
	const alive_bool = alive_check.test(message_content)
	const here_bool = here_check.test(message_content)
	return (up_bool || alive_bool || here_bool)
}

function missing_keyword_test(message)
{
	const message_content = message.content.toLowerCase()
	
	const missing_check = new RegExp("^missing$")
	
	const missing_bool = missing_check.test(message_content)
	return missing_bool
}

function missing_report_check(message)
{
	const message_content = message.content.toLowerCase()
	const channel_name = message.channel.name
	
	const condition1 = missing_keyword_test(message)
	const condition2 = (channel_name === "morning-report")
	return (condition1 && condition2)
}

function report_reply_check(message, client)
{
	const message_content = message.content.toLowerCase()
	const channel_name = message.channel.name
	const author_name = message.author.username
	const bot_name = client.user.username
	
	const condition1 = report_keyword_test(message)
	const condition2 = channel_name === "morning-report"
	const condition3 = author_name != bot_name
	return (condition1 && condition2 && condition3)
}

function setup_accountability_check(message, OBJ)
{
	const class_leaders_dict = OBJ.CLASS_LEADERS
	const class_leaders_id = Object.keys(class_leaders_dict)
	const author_id = message.author.id
	const channel_name = message.channel.name
	const message_content = message.content.toLowerCase()
	
	const condition1 = message_content.startsWith("accountability")
	const condition2 = channel_name === "morning-report"
	const condition3 = class_leaders_id.includes(author_id)
	return (condition1 && condition2 && condition3)
}

module.exports = (client, message) => 
{
	var OBJ = require(process.env.JSON_FILE1)
		
	if (report_reply_check(message, client))
	{
		reportReply(client, message)
	}
	else if (setup_accountability_check(message, OBJ))
	{
		setupAccountability(message)
	}
	else if (missing_report_check(message))
	{
		missingReport(client, message)
	}
}
