const fs = require('fs')

function write_data(OBJ)
{
	var data = JSON.stringify(OBJ)
	fs.writeFileSync(process.env.JSON_FILE1, data)
}

function get_current_time_str()
{
	const today = new Date()
	var current_minute = String(today.getMinutes())
	var current_hour = String(today.getHours())
	if(current_minute.length < 2)
	{
		current_minute = "0" + current_minute
	}
	if(current_hour.length < 2)
	{
		current_hour = "0" + current_hour
	}
	const current_time_str = current_hour+""+current_minute
	return current_time_str
}

function update_json(update_parameters)
{
	OBJ = update_parameters.OBJ
	present_list = update_parameters.present_list
	num_added = update_parameters.num_added
	author_id = update_parameters.author_id
	
	present_list.push(author_id.toString())
	num_added = parseInt(num_added) + 1
	
	OBJ.PRESENT_LIST = present_list
	OBJ.NUM_ADDED = num_added
	
	write_data(OBJ)
	return num_added
}

function ontime_check(message_parameters)
{
	const OBJ = message_parameters.OBJ
	const message_time = message_parameters.message_time
	const date_created = message_parameters.date_created
	const leader_reported = message_parameters.leader_reported
	
	// the leader report is always on time ;)
	if (leader_reported)
	{
		return leader_reported
	}
	
	const report_start_time = OBJ.START_TIME
	const report_end_time = OBJ.END_TIME
	const report_date = OBJ.REPORT_DATE
	const num_added = OBJ.NUM_ADDED
	const num_members = OBJ.NUM_MEMBERS
	 
	const condition1 = report_start_time <= message_time
	const condition2 = message_time <= report_end_time
	const condition3 = report_date == date_created
	const condition4 = message_time >= parseInt(OBJ.START_TIME)
	const condition5 = parseInt(num_added) === parseInt(num_members)
	const condition6 = condition4 && condition5
	return (condition1 && condition2 && condition3 && !condition6)
}

function early_check(message_time, date_created, month_created, OBJ)
{
	const report_start_time = parseInt(OBJ.START_TIME)
	const report_date = OBJ.REPORT_DATE
	const report_month = OBJ.REPORT_MONTH_NUM
	
	const condition1 = message_time < report_start_time
	const condition2 = date_created == report_date
	const condition3 = date_created < report_date
	const condition4 = month_created <= report_month
	return ((condition1 && condition2) || (condition3 && condition4))
}

function time_expired_check(message_time, date_created, month_created, OBJ)
{
	const report_start_time = parseInt(OBJ.START_TIME)
	const report_date = OBJ.REPORT_DATE
	const report_month = OBJ.REPORT_MONTH_NUM
	const expire_after = 1200
	var expire_date = report_date
	var expire_time = report_start_time + expire_after
	var expire_month = report_month
	if(expire_time >= 2400)
	{
		expire_time = expire_time - 2400
		const today = new Date()
		const tomorrow = new Date(today)
		tomorrow.setDate(tomorrow.getDate()+1)
		expire_date = tomorrow.getDate()
		expire_month = tomorrow.getMonth() + 1
	}
	const condition1 = month_created > expire_month
	const condition2 = date_created > expire_date
	const condition3 = message_time > expire_time
	const condition4 = date_created == expire_date
	return (condition1 || condition2 || (condition3 && condition4))
}

function mention_check(message, OBJ)
{
	const author_id = message.author.id
	const mentions = message.mentions.users
	const class_leaders_dict = OBJ.CLASS_LEADERS
	const class_leaders_id = Object.keys(class_leaders_dict)

	const condition1 = class_leaders_id.includes(author_id.toString())
	const condition2 = mentions.array().length > 0
	return (condition1 && condition2)
}

module.exports = (client, message) => 
{
	// import permanent variables
	var OBJ = require(process.env.JSON_FILE1)
	const num_members = parseInt(OBJ.NUM_MEMBERS)
	const all_up = parseInt(OBJ.ALL_UP)
	var num_added = parseInt(OBJ.NUM_ADDED)
	var present_list = OBJ.PRESENT_LIST
	
	//initialize report variables
	const report_month_num = parseInt(OBJ.REPORT_MONTH_NUM)
	const report_date = parseInt(OBJ.REPORT_DATE)
	const report_start_time = OBJ.START_TIME
	const report_end_time = OBJ.END_TIME
	
	// initialize message variables
	var author_id = ""
	var leader_reported = false
	if (mention_check(message, OBJ))
	{
		const mentions = message.mentions.users.keyArray()
		author_id = mentions[0].toString()
		leader_reported = true
	}
	else
	{
		author_id = message.author.id.toString()
	}
	const date_created = message.createdAt.getDate()
	const month_created = message.createdAt.getMonth() + 1
	const message_time = parseInt(get_current_time_str())
	
	//early condition
	if (early_check(message_time, date_created, month_created, OBJ))
	{
		message.reply('You are early to accountability. Report on ' + 
				      report_month_num + '/' + report_date + 
				      ' between ' + report_start_time + ' and ' + 
				      report_end_time + '.')
	}
	//late condition and on-time condition
	else if(!time_expired_check(message_time, date_created, month_created, OBJ))
	{
		var update_parameters = 
		{
			present_list: present_list,
			num_added: num_added,
			author_id: author_id,
			message_time: message_time,
			date_created: date_created,
			leader_reported: leader_reported,
			OBJ: OBJ
		}
		//already reported condition
		const ontime = ontime_check(update_parameters)
		if (present_list.includes(author_id))
		{
			message.reply('You have already reported up to accountability on ' + 
			report_month_num + '/' + report_date + 
			' between ' + report_start_time + ' and ' + 
			report_end_time + '.')
		}
		// on-time condition
		else if(ontime)
		{
			message.reply('thanks! \:thumbsup:')
			num_added = update_json(update_parameters)
		}
		else
		{
			message.reply("You have reported late, " + 
						  "don't let it happen again!")
			num_added = update_json(update_parameters)
		}
		
		//all up check
		if(all_up === 0 && num_added === num_members)
		{
			message.reply("@everyone we're all up! :flag_us:")
			OBJ.LAST_REPORT_DATE = report_date
			OBJ.LAST_REPORT_MONTH_NUM = report_month_num
			OBJ.LAST_END_TIME = get_current_time_str()
			OBJ.ALL_UP = 1
			
			write_data(OBJ)
		}
	}
}
