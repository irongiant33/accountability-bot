const fs = require('fs')

// tests user messages for valid accountability time strings
function time_check(message_content)
{
	const start_time_str = message_content[1]
	const end_time_str = message_content[2]
	
	const start_check = new RegExp("^([01]?[0-9]|2[0-3])[0-5][0-9]$")
	const end_check = new RegExp("^([01]?[0-9]|2[0-3])[0-5][0-9]$")
	
	const start_bool = start_check.test(start_time_str)
	const end_bool = end_check.test(end_time_str)
	return !(start_bool && end_bool)
}

function get_current_time_str()
{
	const today = new Date()
	var current_minute = String(today.getMinutes())
	var current_hour = String(today.getHours())
	if(current_minute.length < 2)
	{
		current_minute = "0" + current_minute
	}
	if(current_hour.length < 2)
	{
		current_hour = "0" + current_hour
	}
	const current_time_str = current_hour+""+current_minute
	return current_time_str
}

function write_data(OBJ)
{
	var data = JSON.stringify(OBJ)
	fs.writeFileSync(process.env.JSON_FILE1, data)
}

module.exports = (message) => 
{
	const message_content = message.content.split(" ")
	const start_time_str = message_content[1]
	const end_time_str = message_content[2]
	
	if (message_content.length === 3)
	{
		const start_time = parseInt(start_time_str)
		const end_time = parseInt(end_time_str)
		//check format of the time parameters
		if (time_check(message_content))
		{
			message.reply("Please enter your start and end time in " + 
						  "military format.")
		}
		//check to see if the start time is greater than the end time
		else if (start_time > end_time)
		{
			message.reply("The start time cannot be after the " +
						  "end time.")
		}
		//process parameters
		else 
		{			
			//calculate what day and time the report will take place
			today = new Date()
			var day_num = today.getDate()
			var month_num = today.getMonth() + 1
			const current_time = parseInt(get_current_time_str())
			if (start_time < current_time)
			{
				const tomorrow = new Date(today)
				tomorrow.setDate(tomorrow.getDate()+1)
				day_num = tomorrow.getDate()
				month_num = tomorrow.getMonth() + 1
			}
			
			var OBJ = require(process.env.JSON_FILE1)
			OBJ.START_TIME = message_content[1]
			OBJ.END_TIME = message_content[2]
			OBJ.REPORT_DATE = day_num
			OBJ.REPORT_MONTH_NUM = month_num
			OBJ.PRESENT_LIST = []
			OBJ.NUM_ADDED = 0
			OBJ.ALL_UP = 0
			write_data(OBJ)
			
			message.reply("@everyone \nAccountability report to " +
						  "follow on " + month_num + "/" + day_num + 
						  ". The start time has been set for " + 
						  OBJ.START_TIME + ". The end time has been " +
						  "set for " + OBJ.END_TIME + ".")
		}
	}
	else
	{
		message.reply("You have the incorrect number of arguments. " + 
					  "The proper usage is: `accountability " + 
					  "startTime endTime` in military format.")
	}
}
