function all_accounted_for_check(message_time, OBJ)
{
	const num_added = OBJ.NUM_ADDED
	const num_members = OBJ.NUM_MEMBERS
	
	const condition1 = message_time < OBJ.START_TIME
	const condition2 = message_time >= parseInt(OBJ.START_TIME)
	const condition3 = parseInt(num_added) === parseInt(num_members)
	const condition4 = condition2 && condition3
	return (condition1 || condition4)
}

function get_current_time_str()
{
	const today = new Date()
	var current_minute = String(today.getMinutes())
	var current_hour = String(today.getHours())
	if(current_minute.length < 2)
	{
		current_minute = "0" + current_minute
	}
	if(current_hour.length < 2)
	{
		current_hour = "0" + current_hour
	}
	const current_time_str = current_hour+""+current_minute
	return current_time_str
}

module.exports = (client, message) => 
{
	// import permanent variables
	var OBJ = require(process.env.JSON_FILE1)
	const report_month = OBJ.REPORT_MONTH_NUM
	const report_date = OBJ.REPORT_DATE
	var member_list = OBJ.MEMBER_LIST
	var present_list = OBJ.PRESENT_LIST
	const num_members = parseInt(OBJ.NUM_MEMBERS)
	const num_added = parseInt(OBJ.NUM_ADDED)
	const current_time_str = get_current_time_str()
	const message_time = parseInt(current_time_str)
	
	var missing_people = []
	const member_id_list = Object.keys(member_list)
	for (i = 0; i < num_members; i++)
	{
		if(!(present_list.includes(member_id_list[i])))
		{
			const member_id = member_id_list[i]
			const member_values = member_list[member_id]
			const member_string = member_values[1] + " - " + member_values[2]
			missing_people.push(member_string)
		}
	}
	
	// all accounted for condition
	if (all_accounted_for_check(message_time, OBJ))
	{
		message.reply("Everyone is accounted for as of " + 
					  OBJ.LAST_REPORT_MONTH_NUM + 
					  "/" + OBJ.LAST_REPORT_DATE + 
					  " at " + OBJ.LAST_END_TIME)
	}
	// not all accounted for
	else 
	{
		const missing_people_str = missing_people.join("\n")
		message.reply(num_added + " out of " + num_members + 
				      " are up as of " + current_time_str + 
				      " on " + report_month + "/" + report_date + ".\n" +
					  "The following people are unaccounted for:\n" +
					  missing_people_str)
	}
}
